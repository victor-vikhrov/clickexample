package com.example.itschool.buttonclick;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button bMinus, bPlus;
    TextView text;

    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bPlus = (Button) findViewById(R.id.b_plus);
        bMinus = (Button) findViewById(R.id.b_minus);
        bPlus.setOnClickListener(this);
        bMinus.setOnClickListener(this);


        counter = 0;
        text = (TextView) findViewById(R.id.tv_text);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_minus : counter--;
                                break;
            case R.id.b_plus : counter++;
                                break;

        }
        text.setText(String.valueOf(counter));
    }
}
